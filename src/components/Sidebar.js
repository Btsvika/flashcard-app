import React from 'react';
import ReactDOM from 'react-dom';
// connect:
// define what property and functions we want a container component to have.
// applay it to a presentation component and get a complete react component
import { connect } from 'react-redux';
import {addDeck, showAddDeck, hideAddDeck} from '../actions';

/*
// Data
decks={state.decks}
addingDeck={state.addingDeck}

// Functions
addDeck={name => store.dispatch(addDeck(name))}
showAddDeck={() => store.dispatch(showAddDeck())}
hideAddDeck={() => store.dispatch(hideAddDeck())}

*/
// the following is simplified due to es6
// const mapStateToProps = (state) => {
//   decks: state.decks,
//   addingDeck: state.addingDeck
// }

const mapStateToProps = ({decks, addingDeck}) => ({
  decks,
  addingDeck
});

const mapDispatchToProps = dispatch => ({
  addDeck: name => dispatch(addDeck(name)),
  showAddDeck: () => dispatch(showAddDeck()),
  hideAddDeck: () => dispatch(hideAddDeck())
});

// this is a presentation component
const Sidebar = React.createClass({
  componentDidUpdate() {
    var el = ReactDOM.findDOMNode(this.refs.add);
    if (el) el.focus();
  },
  render() {
    let props = this.props;

    return (<div className='sidebar'>
      <h2> All Decks</h2>

      <button onClick={ e => this.props.showAddDeck()}>New Deck</button>
      <ul>
        {props.decks.map((deck, i) =>
          <li key={i}> {deck.name} </li>
        )}
      </ul>
      { props.addingDeck && <input ref='add' onKeyPress={this.createDeck} /> }
    </div>)
  },
  createDeck(evt) {
    if(evt.which != 13) return;
    var name = ReactDOM.findDOMNode(this.refs.add).value;
    this.props.addDeck(name);
    this.props.hideAddDeck();

  }
});
// connect() this will return a new function that we execute right away with ()
// export default connect(property-and-functions)(presentational-component);
export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);
