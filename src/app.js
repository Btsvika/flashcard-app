import React from 'react';
import ReactDOM from 'react-dom';
// import * as Redux from 'redux';
import {createStore, combineReducers} from 'redux';
// Provider let to use the store everywhere
import { Provider } from 'react-redux';
import { Router, Route } from 'react-router';
import { createBrowserHistory } from 'history';
import { syncHistoryWithStore, routerReducer } from 'react-router-redux';
// reducers is object with all the reducers as properties
import * as reducers from './reducers';
// routing will be a new property of reducers and routerReducer will be that function
reducers.routing = routerReducer;

import App from './components/App'

const store = createStore( combineReducers(reducers));
// whenever the BrowserHistory will change it will be sync with the store
// and we will use the new history object insted of the raw BrowserHistory
const history = syncHistoryWithStore(createBrowserHistory(), store);


function run() {
  let state = store.getState();
  console.log(state);
  ReactDOM.render((
    <Provider store={store}>
      <Router history={history}>
        <Route path='/' component={App}></Route>
      </Router>
    </Provider>), document.getElementById('root'));
  }

run();
store.subscribe(run);
